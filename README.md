## Getting Started

This website is using NPM as it's main manager

- To start working simply run [ -npm install ] if you haven't done that so to install all the dependencies

- To start the server you need to type [ -npm run dev ] or [ -npm run dev -p your_port ] to start on the your port.

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
