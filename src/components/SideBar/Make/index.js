import React, { useState } from "react";
import { useSelector } from "react-redux";
import styles from "./index.module.scss";
import { FiPlusSquare } from "react-icons/fi";
import { FiMinusSquare } from "react-icons/fi";

const Makes = () => {
	const { makesCount } = useSelector((state) => state.searchFilterCity);
	const [toggle, setToggle] = useState(false);
	return (
		<div className={styles.make_wrapper}>
			<p onClick={() => setToggle(!toggle)}>
				Make{" "}
				{toggle ? (
					<FiMinusSquare className={styles.make_toggle_icon} />
				) : (
					<FiPlusSquare className={styles.make_toggle_icon} />
				)}
			</p>
			{toggle && (
				<div>
					{makesCount.map((item, index) => {
						return (
							<div className={styles.make_checkbox_container}>
								<input type='checkbox' />
								<label>
									{Object.keys(item)} ({Object.values(item)})
								</label>
							</div>
						);
					})}
				</div>
			)}
		</div>
	);
};

export default Makes;
