import React, { useState } from "react";
import styles from "./index.module.scss";
// import Make from "../filterByMake/make";
import City from "./City/index";
import Keyword from "./Keyword/index";
import Makes from "./Make/index";
import Model from "./Model/model";
import Trim from "./Trim/trim";
import { FiPlusSquare } from "react-icons/fi";
import { FiMinusSquare } from "react-icons/fi";

const SideBar = () => {
	const [collapse, setCollapse] = useState(true);
	return (
		<div className={styles.sidebar_main}>
			<div className={styles.sidebar_wrapper}>
				<h3>Sort by:</h3>
				<div className={styles.sidebar_collapse_buttons}>
					<button
						onClick={() => setCollapse(!collapse)}
						className={collapse ? styles.collapse : null}>
						Collapse All <FiMinusSquare style={{ marginLeft: "5px" }} />
					</button>
					<button
						onClick={() => setCollapse(!collapse)}
						className={!collapse ? styles.collapse : null}>
						Expand All <FiPlusSquare style={{ marginLeft: "5px" }} />
					</button>
				</div>
				<div className={styles.top_search}>
					<City />
					<Keyword />
				</div>
				<Makes />
				<Model />
				<Trim />
				{/* <Make /> */}
			</div>
		</div>
	);
};

export default SideBar;
