import React from "react";
import { useDispatch } from "react-redux";
import styles from "./index.module.scss";

const City = () => {
	const dispatch = useDispatch();
	return (
		<div>
			<span>
				<h4>Choose your city:</h4>
				<select
					name='city'
					onChange={(e) => dispatch({ type: "CITY_SELECT", payload: e })}>
					<option defaultValue='All'>All</option>
					<option value='Ottawa'>Ottawa</option>
					<option value='Almaty'>Almaty</option>
					<option value='Nur-Sultan'>Nur-Sultan</option>
				</select>
			</span>
		</div>
	);
};

export default City;
