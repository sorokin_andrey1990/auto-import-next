import React from "react";
import { useDispatch } from "react-redux";
import styles from "./index.module.scss";

const Keyword = () => {
	const dispatch = useDispatch();
	return (
		<div>
			<h4>Search by:</h4>
			<input
				type='text'
				placeholder='ex. Bmw 328i'
				onChange={(e) => dispatch({ type: "SEARCH_BAR", payload: e })}
			/>
		</div>
	);
};

export default Keyword;
