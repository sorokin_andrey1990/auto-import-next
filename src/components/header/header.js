import React from "react";
import Image from "next/image";
import { AiOutlinePhone } from "react-icons/ai";
import { HiOutlineMail } from "react-icons/hi";
import { GoLocation } from "react-icons/go";
import styles from "./header.module.scss";
const Header = () => {
  return (
    <>
      <header className={styles.header}>
        <div className={styles.logo}>
          <Image
            src="/assets/img/logo.jpg"
            alt="Logo"
            width={200}
            height={75}
          />
        </div>
        <div className={styles.contacts}>
          <div className={styles.contact}>
            <AiOutlinePhone className={styles.icon} />
            <a href="tel:+77479256058" type="phone">
              Sales: +7(747)-925-6058
            </a>
          </div>
          <div className={styles.contact}>
            <HiOutlineMail className={styles.icon} />
            <a href="mailto:sorokin.andrey1990@gmail.com" target="_blank">
              Email Us
            </a>
          </div>
          <div className={styles.contact}>
            <GoLocation className={styles.icon} />
            <a href="https://yandex.com/maps/-/CCUAURSocD" target="_blank">
              26 Republic Ave. Nur-Sultan
            </a>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
