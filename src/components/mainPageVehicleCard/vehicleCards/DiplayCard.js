import React from "react";
import Link from "next/link";
import styles from "./displayCards.module.scss";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { useRouter } from "next/router";

const DisplayCar = ({ Odo, ImgUrl, Year, Price, Model, id, Make }) => {
	const router = useRouter();
	return (
		<div className={styles.card} key={id && id}>
			<Link href={`/car/${id && id}`}>
				<img className={styles.car_image} src={`${ImgUrl?.url}`}></img>
			</Link>
			<div className={styles.info_container}>
				<div className={styles.info_car}>
					<h4>
						{Make && Make} {Model && Model}
					</h4>
					<h5>Year: {Year && Year}</h5>
					<h5>Odometer: {Odo && Odo} kms</h5>
				</div>
				<div className={styles.bottom_card_info}>
					<div className={styles.price_devider}>
						<div className={styles.separator}>
							<p>Est. Finance Payment</p>
							<h5>$195/bw</h5>
							<p> 5.99% for 60 Months</p>
						</div>
						<div className={styles.price}>
							<p>Dealer Price</p>
							<span className={styles.car_price_element}>
								<h5>{Price && Price}</h5>
								<AiOutlineExclamationCircle
									className={styles.price_exclamation}
									width={10}
									height={10}
								/>
							</span>

							<p className='tax'>+ Tax</p>
						</div>
					</div>
					<div className={styles.partners}>
						<img
							src='https://www.luxuryautoimports.ca/wp-content/themes/convertus-achilles/achilles/assets/images/car-fax-badge-view-report-en.svg'
							alt='partners'
						/>

						<Link
							href={`/car/${id && id}`}
							params={{ id: id }}
							className={styles.linkComponent}>
							<button className={styles.detailLink}>Details</button>
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
};

export default DisplayCar;
