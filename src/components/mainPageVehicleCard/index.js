import React from "react";
import DisplayCar from "./vehicleCards/DiplayCard";
import { useSelector } from "react-redux";
import styles from "./index.module.scss";

function ShowCar() {
	const { city, filterCar, cars } = useSelector(
		(state) => state.searchFilterCity
	);

	return (
		<div>
			<div className={styles.cityAndSearch}>
				<div className={styles.inventory_header}>
					<h2>
						{city.toLowerCase() === "all"
							? "All vehicles for sale"
							: `Vehicles for sale in ${city}`}
					</h2>
					<h4>{filterCar.length} available</h4>
				</div>
			</div>
			<div className={styles.grid_test}>
				{filterCar.map((item) => {
					return <DisplayCar key={item.id} {...item} />;
				})}
			</div>
		</div>
	);
}

export default ShowCar;
