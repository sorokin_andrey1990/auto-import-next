import React, { useState, useEffect } from "react";
import Link from "next/link";
import PhotoModal from "./photoSlider/photoModal";
import AboutThisVh from "../vehiclePage/aboutVh/aboutThisVh";
import { useSelector } from "react-redux";
import styles from "./index.module.scss";

const VehicleDisplay = ({ match }) => {
	const { cars } = useSelector((state) => state.searchFilterCity);
	const specificVh = cars.filter((item) => item.id === match.id);
	const [modalImage, setModalImage] = useState(null);
	const [showSlider, setShowSlider] = useState(false);
	const [seeMore, setSeeMore] = useState(false);
	const carImages = specificVh[0].Images;
	return (
		<div className={styles.carScreen}>
			<nav className={styles.vhCar}>
				<Link href={"/"}>
					<span>Home</span>
				</Link>
			</nav>
			<section className={styles.specific_car}>
				{specificVh.map((item) => {
					return (
						<div key={item.id}>
							<h2>
								{item.Year} &middot; {item.Model} &middot; {item.Trim} &middot;{" "}
								{item.DriveTrain} &middot; Only {item.Odo} kms
							</h2>
							<div className={styles.img_grid}>
								{item?.Images?.map((itemTwo, index) => {
									if (!seeMore ? index <= 5 : index <= item.Images.length) {
										return (
											<div key={Math.random()} className={styles.img_wrap}>
												<img
													onClick={() => {
														setModalImage(index);
														setShowSlider(true);
													}}
													src={`${itemTwo.url}`}
													alt='Car'
												/>
											</div>
										);
									}
								})}
							</div>
							{specificVh[0]?.Images.length > 6 ? (
								<button
									onClick={() => setSeeMore(!seeMore)}
									className={styles.seeMore_vhDisplay}>
									{!seeMore ? "See more" : "Show less"}
								</button>
							) : null}

							<AboutThisVh item={item} />
						</div>
					);
				})}
			</section>
			{showSlider ? (
				<PhotoModal
					modalImage={modalImage}
					images={carImages}
					setModalImage={setModalImage}
					setShowSlider={setShowSlider}
				/>
			) : null}
		</div>
	);
};

export default VehicleDisplay;
