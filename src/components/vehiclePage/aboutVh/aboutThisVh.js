import React from "react";
import styles from "./style.module.scss";
import { BiTachometer } from "react-icons/bi";
import { HiOutlineClipboardList } from "react-icons/hi";
import { RiCarLine } from "react-icons/ri";
import { RiSteeringLine } from "react-icons/ri";
import { BiCog } from "react-icons/bi";
import { BiGasPump } from "react-icons/bi";
import { FiTruck } from "react-icons/fi";
import { GoLocation } from "react-icons/go";
import { AiOutlinePhone } from "react-icons/ai";
import { HiOutlineMail } from "react-icons/hi";
import { GiCartwheel } from "react-icons/gi";

const AboutThisVh = ({ item }) => {
	return (
		<div className={styles.vehicle_desc_main}>
			<div className={styles.aboutVh}>
				<div className={styles.aboutVh_info_wrap}>
					<FiTruck className={styles.vh_about_icon} />
					<h5>Located at: {item.VhLocation}</h5>
				</div>
				<div className={styles.aboutVh_info_wrap}>
					<GoLocation className={styles.vh_about_icon} />
					<h5>
						<a href='https://yandex.com/maps/-/CCUAURSocD' target='_blank'>
							{item.City}
						</a>
					</h5>
				</div>
				<div className={styles.aboutVh_info_wrap}>
					<AiOutlinePhone className={styles.vh_about_icon} />
					<h5>
						<a href='tel:+77479256058' type='phone'>
							Sales: +7(747)-925-6058
						</a>
					</h5>
				</div>
				<div className={styles.aboutVh_info_wrap}>
					<HiOutlineMail className={styles.vh_about_icon} />
					<h5>
						<a href='mailto:shadow198rus@gmail.com'>Email Us</a>
					</h5>
				</div>
			</div>
			<h1 style={{ textAlign: "left", margin: "1.8rem 0 15px 0" }}>
				About this vehicle
			</h1>
			<p style={{ textAlign: "left", marginBottom: 5, fontSize: 15 }}>
				Price: ${item.Price}
			</p>
			<p style={{ textAlign: "left", fontSize: 14, marginTop: 0 }}>
				VIN: {item.Vin}
			</p>
			<div className={styles.aboutVh_wrapper}>
				<div className={styles.aboutVh_card}>
					<BiTachometer className={styles.vh_card_icon} />
					<h5>Kilometres</h5>
					<p>{item.Odo}</p>
				</div>
				<div className={styles.aboutVh_card}>
					<HiOutlineClipboardList className={styles.vh_card_icon} />
					<h5>Condition</h5>
					<p>{item.Condition}</p>
				</div>
				<div className={styles.aboutVh_card}>
					<RiCarLine className={styles.vh_card_icon} />
					<h5>Body Style</h5>
					<p>{item.BodyStyle}</p>
				</div>
				<div className={styles.aboutVh_card}>
					<BiCog className={styles.vh_card_icon} />
					<h5>Engine</h5>
					<p>{item.Engine}</p>
				</div>
				<div className={styles.aboutVh_card}>
					<BiGasPump className={styles.vh_card_icon} />
					<h5>Fuel Type</h5>
					<p>{item.FuelType}</p>
				</div>
				<div className={styles.aboutVh_card}>
					<RiSteeringLine className={styles.vh_card_icon} />
					<h5>Transmission</h5>
					<p>{item.Transmission}</p>
				</div>
				<div className={styles.aboutVh_card}>
					<GiCartwheel className={styles.vh_card_icon} />
					<h5>Drive Train</h5>
					<p>{item.DriveTrain}</p>
				</div>
				<div className={styles.aboutVh_card}>
					<BiGasPump className={styles.vh_card_icon} />
					<h5>Fuel Economy</h5>
					<p>{item.FuelEconomy}</p>
				</div>
			</div>
		</div>
	);
};

export default AboutThisVh;
