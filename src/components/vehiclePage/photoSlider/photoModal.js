import React from "react";
import styles from "./photoModal.module.scss";

const PhotoModal = ({ modalImage, images, setModalImage, setShowSlider }) => {
	const carToDisplay = images.filter((item, index) => index == modalImage);

	const handleClick = (e) => {
		if (e.target.tagName == "DIV") {
			setShowSlider(false);
		}
	};
	return (
		<div
			className={styles.modal_div_for_car}
			onClick={(e) => {
				handleClick(e);
			}}>
			<div className={styles.innerDiv} style={{ position: "relative" }}>
				<img
					className={styles.modalCarImage}
					src={`${carToDisplay[0].url}`}
					alt=''
				/>
				<span
					className={styles.next_image}
					onClick={() => {
						if (modalImage == images.length - 1) {
							setModalImage(0);
						} else {
							setModalImage(modalImage + 1);
						}
					}}>
					Next image
				</span>
				<span
					className={styles.prev_image}
					onClick={() => {
						if (modalImage == 0) {
							setModalImage(images.length - 1);
						} else {
							setModalImage(modalImage - 1);
						}
					}}>
					Prev image
				</span>
			</div>
		</div>
	);
};

export default PhotoModal;
