import React from "react";
import styles from "./index.module.scss";

const Footer = () => {
	return (
		<div className={styles.footer_wrapper}>
			Andrey Sorokin @{" "}
			<a href='mailto:sorokin.andrey1990@gmail.com' target='_blank'>
				mailto
			</a>
		</div>
	);
};

export default Footer;
