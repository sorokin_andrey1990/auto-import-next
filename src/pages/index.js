import React from "react";
import Layout from "../components/layout/Layout";
import axios from "axios";
import { useDispatch } from "react-redux";
import SideBar from "../components/SideBar/index";
import ShowCar from "../components/mainPageVehicleCard/index";

export default function Home({ cars }) {
	const dispatch = useDispatch();
	cars && dispatch({ type: "CARS_ARRAY", payload: cars });

	return cars ? (
		<Layout>
			<SideBar />
			<ShowCar />
		</Layout>
	) : (
		<Layout>
			<h2>
				Ooopss.. most likely there was an error fetching data, we are working on
				it..
			</h2>
		</Layout>
	);
}

export const getServerSideProps = async () => {
	const api = axios.create({
		baseURL: process.env.BASE_URL,
	});
	const cars = await api
		.get("cars")
		.then((res) => res.data)
		.catch((err) => false);

	return {
		props: {
			cars,
		},
	};
};
