import React from "react";
import styles from "./car.module.scss";
import VehicleDisplay from "../../components/vehiclePage";
import Layout from "../../components/layout/Layout";

const Car = (params) => {
	return (
		<Layout>
			<VehicleDisplay match={params} />
		</Layout>
	);
};

export default Car;

export const getServerSideProps = async ({ params }) => {
	return {
		props: params,
	};
};
