// ALL REDUCERS IN THIS FILE HAVE INTERCONNECTIONS AND WORK IN COLLABORATION WITH EACHOTHER. THEY ALL RELY ON THE SAME PIECE OF STORE/DATA. IF YOU HAVE INTENSION TO SEPARATE ACTIONS INTO SEPARATE REDUCERS, YOU NEED TO CONNECT THEM TO THE SAME STORE SO THEY USE SAME DATA OR COMPLETELY RE-THINK THE LOGIC OF THE COMPONENT//

const initialState = {
  cars: [],
  filterCar: [],
  allMakes: [],
  city: "",
  mainModal: true,
  makesCount: [],
};

export default function searchFilterCity(state = initialState, action) {
  //----------- MAIN MODAL WINDOW -----------//
  if (action.type === "MODAL_OFF") {
    return {
      ...state,
      mainModal: action.payload,
    };
  }

  //-----------------------------------------//
  //------------- INITIAL ARRAY -------------//
  //-----------------------------------------//

  if (action.type === "CARS_ARRAY") {
    let finish = [];
    const makes = [...new Set(action.payload.map(item => item.Make))];
    for (let i = 0; i < makes.length; i++) {
      const count = action.payload.filter(item => item.Make === makes[i]);
      const val = Object.fromEntries(new Map([[count[0]?.Make, count.length]]));
      finish.push(val);
    }
    return {
      ...state,
      cars: action.payload,
      filterCar: action.payload,
      allMakes: makes,
      makesCount: finish,
    };
  }

  //-------------------------------------------------//
  //------------- CITY SELECT COMPONENT -------------//
  //-------------------------------------------------//

  if (action.type === "CITY_SELECT") {
    if (action.payload.target.value === "All") {
      return {
        ...state,
        filterCar: state.cars,
        city: action.payload.target.value,
      };
    }
    if (action.payload.target.value === "Almaty") {
      const newCars = state.cars.filter(
        item => item.City === action.payload.target.value
      );
      return {
        ...state,
        filterCar: newCars,
        city: action.payload.target.value,
      };
    }
    if (action.payload.target.value === "Ottawa") {
      const newCars = state.cars.filter(
        item => item.City === action.payload.target.value
      );
      return {
        ...state,
        filterCar: newCars,
        city: action.payload.target.value,
      };
    }
    if (action.payload.target.value === "Nur-Sultan") {
      const newCars = state.cars.filter(
        item => item.City === action.payload.target.value
      );
      return {
        ...state,
        filterCar: newCars,
        city: action.payload.target.value,
      };
    }
  }
  //-----------------------------------------------------//
  //--------------- FILTER BY BUTTON PRESS --------------//
  //-----------------------------------------------------//

  if (action.type === "FILTER_ITEM") {
    const makes = action.payload;
    if (state.city === "") {
      if (makes === "All") {
        return {
          ...state,
          filterCar: state.cars,
        };
      } else {
        const newCars = state.cars.filter(item => item.Make === makes);
        return {
          ...state,
          filterCar: newCars,
        };
      }
    }

    if (makes === "All" && state.city === "All") {
      return {
        ...state,
        filterCar: state.cars,
      };
    }
    if (state.city === "Almaty") {
      const newCars = state.cars.filter(item => item.City === "Almaty");
      const filteredCars = newCars.filter(item => item.Make === makes);
      if (makes === "All") {
        return {
          ...state,
          filterCar: newCars,
        };
      }
      return {
        ...state,
        filterCar: filteredCars,
      };
    }

    if (state.city === "Nur-Sultan") {
      const newCars = state.cars.filter(item => item.City === "Nur-Sultan");
      const filteredCars = newCars.filter(item => item.Make === makes);
      if (makes === "All") {
        return {
          ...state,
          filterCar: newCars,
        };
      }
      return {
        ...state,
        filterCar: filteredCars,
      };
    }
    if (state.city === "Ottawa") {
      const newCars = state.cars.filter(item => item.City === "Ottawa");
      const filteredCars = newCars.filter(item => item.Make === makes);
      if (makes === "All") {
        return {
          ...state,
          filterCar: newCars,
        };
      }
      return {
        ...state,
        filterCar: filteredCars,
      };
    }
    if (state.city === "All") {
      const newCars = state.cars.filter(item => item.Make === makes);

      return {
        ...state,
        filterCar: newCars,
      };
    }
  }

  //-----------------------------------------------------//
  //--------------- SEARCH INPUT COMPONENT --------------//
  //-----------------------------------------------------//

  if (action.type === "SEARCH_BAR") {
    if (state.city === "Almaty") {
      const newCars = state.cars.filter(item => item.City === "Almaty");

      const filteredCars = newCars.filter(
        item =>
          item.Model.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          ) ||
          item.Make.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          )
      );
      return {
        ...state,
        filterCar: filteredCars,
      };
    }
    if (state.city === "Ottawa") {
      const newCars = state.cars.filter(item => item.City === "Ottawa");

      const filteredCars = newCars.filter(
        item =>
          item.Model.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          ) ||
          item.Make.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          )
      );
      return {
        ...state,
        filterCar: filteredCars,
      };
    }
    if (state.city === "Nur-Sultan") {
      const newCars = state.cars.filter(item => item.City === "Nur-Sultan");
      const filteredCars = newCars.filter(
        item =>
          item.Model.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          ) ||
          item.Make.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          )
      );
      return {
        ...state,
        filterCar: filteredCars,
      };
    }
    if (state.city === "All") {
      const newCars = state.cars.filter(item => {
        return (
          item.Model.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          ) ||
          item.Make.toLowerCase().includes(
            action.payload.target.value.toLowerCase()
          )
        );
      });
      return {
        ...state,
        filterCar: newCars,
      };
    }
    return {
      ...state,
    };
  }

  return initialState;
}
