import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import searchFilterCity from "./searchFilterCity/searchFilterCityReducer";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["searchFilterCity"],
};

const rootReduccer = combineReducers({ searchFilterCity });

export default persistReducer(persistConfig, rootReduccer);
